
class CervixUteri:

    @staticmethod
    def create_cervix_uteri_classification_list():
        cervix_uteri_staging = list()
        cervix_uteri_staging.append("The carcinoma is strictly confined to the cervix uteri: 1")
        cervix_uteri_staging.append("The carcinoma invades beyond the uterus, "
                                    "but has not extended onto the lower third of the vagina or to the pelvic wall: 2")
        cervix_uteri_staging.append("The carcinoma involves the lower third of the vagina and/or "
                                    "extends to the pelvic wall "
                                    "and/or causes hydronephrosis or non‐functioning kidney and/or "
                                    "involves pelvic and/or paraaortic lymph nodes: 3")
        cervix_uteri_staging.append("The carcinoma has extended beyond the true pelvis or "
                                    "has involved (biopsy proven) the mucosa of the bladder or rectum."
                                    "A bullous edema, as such, does not permit a case to be allotted to stage IV: 4")

        return cervix_uteri_staging

    @staticmethod
    def select_cervix_uteri_characteristics():
        """
        Provides a list of characteristics of cervix uteri for user to select

        """
        print("")

        cer_ute_staging = CervixUteri.create_cervix_uteri_classification_list()

        for i in range(len(cer_ute_staging)):
            print(cer_ute_staging[i])

        print("")

        staging = int(input("Select staging: "))

        print("")

        if staging == 1:
            print(" ")
            CervixUteri.get_stage1_classification()

        if staging == 2:
            print(" ")
            CervixUteri.get_stage2_classification()

        if staging == 3:
            print("")
            CervixUteri.get_stage3_classification()

        if staging == 4:
            print("")
            CervixUteri.get_stage4_classification()

    @staticmethod
    def get_stage1_classification():
        """
        Prints to the console cervix uteri stage 1 classification

        """
        stromal_size = int(input("Enter measured deepest invasion(mm): "))

        if stromal_size < 3:
            print("Stage IA1")

        if 3 < stromal_size < 5:
            print("Stage IA2")

        if 5 <= stromal_size < 20:
            print("Stage IB1")

        if 20 <= stromal_size < 40:
            print("Stage IB2")

        if stromal_size >= 40:
            print("Stage IB3")

    @staticmethod
    def get_stage2_classification():
        """
        Prints to the console cervix uteri stage 2 classification

        """
        stage2_classification = list()
        stage2_classification.append("Involvement limited to the upper two‐thirds of the "
                                     "vagina without parametrial involvement: 1")
        stage2_classification.append("With parametrial involvement but not up to the pelvic wall: 2")

        for i in range(len(stage2_classification)):
            print(stage2_classification[i])

        print(" ")

        sub_stage = int(input("Enter Number to Select Sub-stage:"))

        if sub_stage == 1:
            carcinoma_size = int(input("Enter invasive carcinoma in greatest dimension(cm): "))

            print(" ")

            if carcinoma_size < 4:
                print("Stage IIA1")

            else:
                print("Stage IIA2")

        else:
            print("Stage IIB")

    @staticmethod
    def get_stage3_classification():
        """
        Prints to console cervix uteri stage 3 classification

        """
        stage3_classification = list()

        stage3_classification.append("Carcinoma involves the lower third of the vagina, "
                                     "with no extension to the pelvic wall: 1")
        stage3_classification.append("Extension to the pelvic wall and/or hydronephrosis "
                                     "or non‐functioning kidney (unless known to be due to another cause): 2")
        stage3_classification.append("Involvement of pelvic and/or paraaortic lymph nodes, "
                                     "irrespective of tumor size and extent (with r and p notations): 3")

        for i in range(len(stage3_classification)):
            print(stage3_classification[i])

        print("")

        sub_stage = int(input("Enter number to select Sub-Stage:"))

        if sub_stage == 1:
            print("Stage IIIA")

        if sub_stage == 2:
            print("Stage IIIB")

        if sub_stage == 3:
            sub_classification = list()
            sub_classification.append("Pelvic lymph node metastasis only: 1")
            sub_classification.append("Paraaortic lymph node metastasis: 2")

            for i in range(len(sub_classification)):
                print(sub_classification[i])

            print("")

            sub_stage = int(input("Enter number to select Sub-Stage:"))

            if sub_stage == 1:
                print("Stage IIIC1")

            else:
                print("Stage IIIC2")

    @staticmethod
    def get_stage4_classification():
        """
        Prints to console cervix uteri stage 4 classification
        
        """
        stage4_classification = list()
        stage4_classification.append("Spread of the growth to adjacent organs: 1")
        stage4_classification.append("Spread to distant organs: 2")

        for i in range(len(stage4_classification)):
            print(stage4_classification[i])

        print("")

        sub_stage = int(input("Enter number to select Sub-Stage:"))

        if sub_stage == 1:
            print("Stage IVA")

        else:
            print("Stage IVB")
