from cervix_uteri_classification import CervixUteri
from ovarian_classification import Ovarian
from vulvar_classification import Vulvar
from endometrial_classification import Endometrium
from vaginal_classification import Vagina
from gestational_classification import Gestation

reproductive_system_list = list()
reproductive_system_list.append("Cervix Uteri: 1")
reproductive_system_list.append("Endometrium: 4")
reproductive_system_list.append("Gestational Trophoblastic Neoplasia: 6")
reproductive_system_list.append("Ovary, Peritoneum & Fallopian Tube: 2")
# reproductive_system_list.append("Uterine Sarcoma: 5")
reproductive_system_list.append("Vagina: 5")
reproductive_system_list.append("Vulva: 3")


def main():
    print("FIGO Classifier \n")

    for i in range(len(reproductive_system_list)):
        print(reproductive_system_list[i])

    print(" ")

    user_input = False
    body_part = 0

    while user_input is False:
        body_part = int(input("Select number to proceed with staging classification:"))

        if is_valid_entry(body_part):
            user_input = True

        else:
            print("There are no options for this number, please enter valid number: ")
            continue

    if body_part == 1:
        CervixUteri.select_cervix_uteri_characteristics()

    if body_part == 2:
        Ovarian.select_ovarian_characteristics()

    if body_part == 3:
        Vulvar.select_vulvar_characteristics()

    if body_part == 4:
        Endometrium.select_endometrial_characteristics()

    if body_part == 5:
        Vagina.select_vaginal_classification()


def is_valid_entry(body_part):
    if 1 > body_part > 2:
        return False
    else:
        return True


if __name__ == "__main__":
    main()
