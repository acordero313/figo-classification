
class Gestation:

    @staticmethod
    def create_gestational_classification_list():
        gestational_staging = list()
        gestational_staging.append("Disease confined to uterus: 1")
        gestational_staging.append("Gestational Trophoblastic tumor extending outside uterus but"
                                   "limited to genital structures (adnexa, vagina, broad ligament): 2")
        gestational_staging.append("Gestational Trophoblastic disease extending to lungs with or "
                                   "without known genital tract involvement: 3")
        gestational_staging.append("All other metastatic sites: 4")

        return gestational_staging

    @staticmethod
    def select_gestational_characteristics():
        gestation_staging = Gestation.create_gestational_classification_list()

        for i in range(len(gestation_staging)):
            print(gestation_staging[i])

        print("")

        staging = int(input("Select staging: "))

        if staging == 1:
            Gestation.get_stage1_classification()

        if staging == 2:
            Gestation.get_stage2_classification()

        if staging == 3:
            Gestation.get_stage3_classification()

        if staging == 4:
            Gestation.get_stage4_classification()

    @staticmethod
    def get_stage1_classification():
        stage1_characteristics = list()
        stage1_characteristics.append("Disease confined to uterus with no risk factors: 1")
        stage1_characteristics.append("Disease confined to uterus with one risk factor: 2")
        stage1_characteristics.append("Disease confined to uterus with two risk factors: 3")

        for i in range(len(stage1_characteristics)):
            print(stage1_characteristics[i])

        staging = int(input("Select number for staging: "))

        if staging == 1:
            print("Stage IA")

        if staging == 2:
            print("Stage IB")

        if staging == 3:
            print("Stage IC")

    @staticmethod
    def get_stage2_classification():
        stage2_characteristics = list()
        stage2_characteristics.append("Gestational trophoblastic tumor involving genital"
                                      "structures without risk factors: 1")
        stage2_characteristics.append("Gestational trophoblastic tumor extending outside uterus but"
                                      "limited to genital structures with one risk factor: 2")
        stage2_characteristics.append("Gestational trophoblastic tumor extending outside uterus but"
                                      "limited to genital structures with two risk factor: 3")

        for i in range(len(stage2_characteristics)):
            print(stage2_characteristics[i])

        staging = int(input("Select number for staging: "))

        if staging == 1:
            print("Stage IIA")

        if staging == 2:
            print("Stage IIB")

        if staging == 3:
            print("Stage IIC")

    @staticmethod
    def get_stage3_classification():
        stage3_characteristics = list()
        stage3_characteristics.append("Gestational trophoblastic tumor extending to lungs with "
                                      "or without genital tract involvement and with no risk factors: 1")
        stage3_characteristics.append("Gestational trophoblastic tumor extending to lungs with "
                                      "or without genital tract involvement and with one risk factors: 2")
        stage3_characteristics.append("Gestational trophoblastic tumor extending to lungs with "
                                      "or without genital tract involvement and with two risk factors: 3")

        for i in range(len(stage3_characteristics)):
            print(stage3_characteristics[i])

        staging = int(input("Select number for staging: "))

        if staging == 1:
            print("Stage IIIA")

        if staging == 2:
            print("Stage IIIB")

        if staging == 3:
            print("Stage IIIC")

    @staticmethod
    def get_stage4_classification():
        stage4_characteristics = list()
        stage4_characteristics.append("All other metastatic sites without risk factors: 1")
        stage4_characteristics.append("All other metastatic sites with one risk factors: 2")
        stage4_characteristics.append("All other metastatic sites with two risk factors: 3")

        for i in range(len(stage4_characteristics)):
            print(stage4_characteristics[i])

        staging = int(input("Select number for staging: "))

        if staging == 1:
            print("Stage IVA")

        if staging == 2:
            print("Stage IVB")

        if staging == 3:
            print("Stage IVC")