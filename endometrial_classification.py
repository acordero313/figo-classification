
class Endometrium:
    @staticmethod
    def endometrial_classification_list():
        endometrial_staging = list()
        endometrial_staging.append("Tumor confined to the corpus uteri: 1")
        endometrial_staging.append("Tumor invades cervical stroma, but does not "
                                   "extend beyond the uterus: 2")
        endometrial_staging.append("Local and/or regional spread of the tumor: 3")
        endometrial_staging.append("Tumor invades bladder and/or bowel mucosa: 4")

        return endometrial_staging

    @staticmethod
    def select_endometrial_characteristics():
        """
        Provides a list of characteristics of endometirum for user to select

        """
        endometrial_staging = Endometrium.endometrial_classification_list()

        for i in range(len(endometrial_staging)):
            print(endometrial_staging[i])

        print("")

        staging = int(input("Select staging: "))

        print("")

        if staging == 1:
            print("")

            Endometrium.get_stage1_classification()

        if staging == 2:
            print("")

            Endometrium.get_stage2_classification()

        if staging == 3:
            print("")

            Endometrium.get_stage3_classification()

        if staging == 4 :
            print("")

            Endometrium.get_stage4_classification()

    @staticmethod
    def get_stage1_classification():
        """
        Prints to the console endometrium stage 1 classification

        """
        stage1_classification = list()
        stage1_classification.append("No or less than half myometrial invasion: 1")
        stage1_classification.append("Invasion equal to or more than half of myometrium: 2")

        for i in range(len(stage1_classification)):
            print(stage1_classification[i])

        print("")

        staging = int(input("Enter number to select staging: "))

        if staging == 1:
            print("Stage IA")
        else:
            print("Stage IB")

    @staticmethod
    def get_stage2_classification():
        """
        Prints to the console endometrium stage 2 classification

        """
        print("Stage II")

    @staticmethod
    def get_stage3_classification():
        """
        Prints to the console endometrium stage 3 classification

        """
        stage3_classification = list()
        stage3_classification.append("Tumor invades the serosa of the corpus uteri and/or adnexae: 1")
        stage3_classification.append("Vaginal and/or parametrial involvement: 2")
        stage3_classification.append("Metastases to pelvic and/or para-aortic lymph nodes: 3")

        for i in range(len(stage3_classification)):
            print(stage3_classification[i])

            print("")

        staging = int(input("Enter number to select staging: "))

        if staging == 1:
            print("Stage IIIA")

        if staging == 2:
            print("Stage IIIB")

        if staging == 3:
            print("Positive pelvic nodes: 1")
            print("Positive para-aortic lymph nodes with or without positive pelvic"
                    "lymph nodes: 2")

            stage_three = int(input("Enter number to select staging: "))

            if stage_three == 1:
                print("Stage IIIC1")
            else:
                print("Stage IIIC2")

    @staticmethod
    def get_stage4_classification():
        """
        Prints to the console endometrium stage 4 classification

        """
        stage4_classification = list()
        stage4_classification.append("Tumor invasion of bladder and/or bowel mucosa: 1")
        stage4_classification.append("Distant metastases, including intra-abdominal "
                                     "metastases and/or inguinal lymph nodes: 2")

        for i in range(len(stage4_classification)):
            print(stage4_classification[i])

            print("")

        staging = int(input("Enter number to select staging: "))

        if staging == 1:
            print("Stage IVA")
        else:
            print("Stage IVB")
