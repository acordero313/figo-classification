
class Vulvar:

    @staticmethod
    def create_vulvar_classification_list():
        vulvar_staging = list()
        vulvar_staging.append("The cancer has formed but has not moved beyond the "
                              "vulva or perineum (the area between the rectum and the vagina.): 1")
        vulvar_staging.append("The cancer has reached the lower part of the urethra, the lower part of the vagina, "
                              "or the anus. The cancer has not reached the lymph nodes: 2")
        vulvar_staging.append("The cancer may have reached the lower part of the urethra, "
                              "the lower part of the vagina, or the anus. Cancer has reached at least one lymph node: 3")
        vulvar_staging.append("The cancer has reached the upper part of the urethra, "
                              "the upper part of the vagina, or other parts of the body: 4")

        return vulvar_staging

    @staticmethod
    def select_vulvar_characteristics():
        """
        Provides a list of characteristics of ovarian for user to select

        """
        print("")

        staging = Vulvar.create_vulvar_classification_list()

        for i in range(len(staging)):
            print(staging[i])

            print("")

            staging = input("Select Staging: ")

            print("")

            if staging == "1":
                print("")
                Vulvar.get_stage1_classification()

            if staging == "2":
                print("")
                Vulvar.get_stage2_classification()

            if staging == "3":
                print("")
                Vulvar.get_stage3_classification()

            if staging == "4":
                print("")
                Vulvar.get_stage4_classification()

    @staticmethod
    def get_stage1_classification():
        """
        Prints to the console ovarian stage 1 classification

        """

        cancer_size = int(input("Enter the size of the cancer in cm: "))

        if cancer_size <= 2:
            cancer_spread = input("Has cancer spread more than 1 mm into the vulva’s tissue (Y/N): ")
            cancer_spread.lower()

            if cancer_spread == "y":
                print("Stage Ia")
        else:
            print("Stage Ib")

    @staticmethod
    def get_stage2_classification():
        """
        Prints to the console ovarian stage 2 classification

        """

        print("Stage II")

    @staticmethod
    def get_stage3_classification():
        """
        Prints to the console ovarian stage 3 classification

        """

        num_lymph_nodes = int(input("How many lymph nodes has the cancer reached? : "))
        size_of_cancer = int(input("Enter size of the cancer in mm: "))

        if num_lymph_nodes <= 2 and size_of_cancer <= 5:
            print("Stage IIIa")
            return

        if num_lymph_nodes <= 1 and size_of_cancer >= 5:
            print("Stage IIIa")
            return

        if num_lymph_nodes >= 3 and size_of_cancer <= 5:
            print("Stage IIIb")
            return

        if num_lymph_nodes >= 2 and size_of_cancer <= 5:
            print("Stage IIIb")
            return

    @staticmethod
    def get_stage4_classification():
        """
        Prints to the console ovarian stage 4 classification

        """
        stage4_classification = list()
        stage4_classification.append("Cancer has reached the lining of the upper urethra, the upper vagina,"
                                     "the bladder, "
                                     "or the rectum, or has embedded in the pelvic bone, "
                                     "OR cancer has reached nearby lymph nodes and the lymph nodes "
                                     "cannot be moved or have formed an ulcer: 1")
        stage4_classification.append("Cancer has reached the lymph nodes of the pelvis or other parts of the body: 2")

        for i in range(len(stage4_classification)):
            print(stage4_classification[i])

        staging = input("Enter number to select staging: ")

        if staging == "1":
            print("Stage IVA")
        else:
            print("IVB")
