
class Ovarian:

    @staticmethod
    def create_ovarian_classification_list():
        ovarian_staging = list()
        ovarian_staging.append("Tumor confined to ovaries: 1")
        ovarian_staging.append("Tumor involves 1 or both ovaries with pelvic extension "
                               "(below the pelvic brim) or primary peritoneal cancer: 2")
        ovarian_staging.append("Tumor involves 1 or both ovaries with cytologically or histologically "
                               "confirmed spread to the peritoneum outside the pelvis and/or metastasis to"
                               "the retoperitoneal lynph nodes: 3")
        ovarian_staging.append("Distant metastasis excluding peritoneal metastasis: 4")

        return ovarian_staging

    @staticmethod
    def select_ovarian_characteristics():
        """
        Provides a list of characteristics of ovarian for user to select

        """
        ovarian_staging = Ovarian.create_ovarian_classification_list()

        for i in range(len(ovarian_staging)):
            print(ovarian_staging[i])

        print("")

        staging = int(input("Select staging: "))

        print("")

        if staging == 1:
            print("")

            Ovarian.get_stage1_classification()

        if staging == 2:
            print("")

            Ovarian.get_stage2_classification()

        if staging == 3:
            print("")

            Ovarian.get_stage3_classification()

        if staging == 4:
            print("")

            Ovarian.get_stage4_classification()

    @staticmethod
    def get_stage1_classification():
        """
        Prints to the console ovarian stage 1 classification

        """
        stage1_classification = list()
        stage1_classification.append("Tumor limited to 1 ovary, capsule intact, no tumors on surface"
                                     "negative washings: 1")
        stage1_classification.append("Tumor involves both ovaries, otherwise like IA: 2")
        stage1_classification.append("Surgical spill: 3")
        stage1_classification.append("Capsule rupture before surgery or tumor on ovarian surface: 4")
        stage1_classification.append("Malignant cells in the ascites or peritoneal washings: 5")

        for i in range(len(stage1_classification)):
            print(stage1_classification[i])

        print("")

        staging = int(input("Enter number to select staging:"))

        if staging == 1:
            print("Stage IA")

        if staging == 2:
            print("Stage IB")

        if staging == 3:
            print("Stage IC1")

        if staging == 4:
            print("Stage IC2")

        if staging == 5:
            print("Stage IC3")

    @staticmethod
    def get_stage2_classification():
        """
        Prints to the console ovarian stage 2 classification

        """

        stage2_classification = list()
        stage2_classification.append("Extension and/or implant on uterus and/or Fallopian tubes: 1")
        stage2_classification.append("Extension to other pelvic intraperitoneal tissues: 2")

        staging = int(input("Enter number to select Sub-Stage:"))

        if staging == 1:
            print("Stage IIA")
        else:
            print("Stage IIB")

    @staticmethod
    def get_stage3_classification():
        """
        Prints to the console ovarian stage 3 classification

        """

        stage3_classification = list()
        stage3_classification.append("Positive retroperitoneal lymph nodes only: 1")
        stage3_classification.append("Microscopic, extrapelvic (above the brim) peritoneal involvement"
                                     " positive retroperitoneal lymph nodes: 2")
        stage3_classification.append("Macroscopic, extrapelvic, peritoneal metastasis <= 2cm"
                                     " positive retroperitoneal lymph nodes Includes extension to capsule"
                                     "of liver/spleen: 3")
        stage3_classification.append("Macroscopic, extrapelvic, peritoneal metastasis > 2cm"
                                     " positive retroperitoneal lymph nodes. Includes extension to capsule"
                                     " to liver/spleen: 4")

        for i in range(len(stage3_classification)):
            print(stage3_classification[i])

        staging = int(input("Enter number to select Sub-Stage:"))

        if staging == 1:
            metastasis_size = int(input("Enter size of metastasis (mm): "))

            if metastasis_size <= 10:
                print("Stage IIIA1(i)")
            else:
                print("Stage IIIA1(ii")

        if staging == 2:
            print("Stage IIIA2")

        if staging == 3:
            print("Stage IIIB")

        if staging == 4:
            print("Stage IIIC")

    @staticmethod
    def get_stage4_classification():
        """
        Prints to the console ovarian stage 4 classification

        """
        stage4_classification = list()
        stage4_classification.append("Pleural effusion with positive cytology: 1")
        stage4_classification.append("Hepatic and/or splenic parenchymal metastasis, metastasis"
                                     "to extra-abdominal organs (including inginual lymph nodes"
                                     "outside of the abdominal cavity: 2")

        for i in range(len(stage4_classification)):
            print(stage4_classification[i])

        staging = int(input("Enter number to select staging: "))

        if staging == 1:
            print("Stage IVA")
        else:
            print("IVB")
