
class Vagina:
    @staticmethod
    def vaginal_classification_list():
        vaginal_classification = list()
        vaginal_classification.append("The carcinoma is limited to the vaginal wall: 1")
        vaginal_classification.append("The carcinoma has involved the sub-vaginal tissue but has"
                                      "not extended to the vaginal wall: 2")
        vaginal_classification.append("The carcinoma has extended to the pelvic wall: 3")
        vaginal_classification.append("The carcinoma has extended beyond the true pelvis or has"
                                      "involved the mucosa of the bladder or rectum: bullouts edema"
                                      "as such does not permit a case to be allotted to Stage: 4")

        return vaginal_classification

    @staticmethod
    def select_vaginal_classification():
        """
        Provides a list of characteristics of the vagina for user to select

        """
        vaginal_staging = Vagina.vaginal_classification_list()

        for i in range(len(vaginal_staging)):
            print(vaginal_staging[i])

        staging = int(input("Select staging: "))

        if staging == 1:
            Vagina.get_stage1_classification()

        if staging == 2:
            Vagina.get_stage2_classification()

        if staging == 3:
            Vagina.get_stage3_classification()

        if staging == 4:
            Vagina.get_stage4_classification()

    @staticmethod
    def get_stage1_classification():
        """
        Prints to the console vaginal stage 1 classification

        """
        print("Stage I")

    @staticmethod
    def get_stage2_classification():
        """
        Prints to the console vaginal stage 2 classification

        """
        print("Stage II")

    @staticmethod
    def get_stage3_classification():
        """
        Prints to the console vaginal stage 3 classification

        """
        print("Stage III")

    @staticmethod
    def get_stage4_classification():
        """
        Prints to the console vaginal stage 4 classification

        """
        stage3_classification_list = list()
        stage3_classification_list.append("Tumor invades bladder and/or rectal mucosa and/or"
                                          "direct extension beyond true pelvis: 1")
        stage3_classification_list.append("Spread to distant organs: 2")

        for i in range(len(stage3_classification_list)):
            print(stage3_classification_list[i])

        staging = int(input("Enter number to select staging: "))

        if staging == 1:
            print("Stage IVA")
        else:
            print("Stage IVB")
